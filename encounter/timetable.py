from typing import List
import json
from urllib.request import Request, urlopen, URLError

from .models import Courses, Enrollments

class Schedule:
    code = ''
    weekday = 0
    period = 0
    semester = ''
    room = ''
    name = ''

day_of_week = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

def extract_from_iniadded(jsonstr) -> List[Schedule]:
    ret = []
    obj = json.loads(jsonstr)
    for item in obj:
        sch = Schedule()
        # {"period":"Mon5","semester":"Fall","id":"1F10402001","name":"コンピュータ・サイエンス概論Ⅱ①,②","classroom":"2313"}
        sch.code = item["id"]
        sch.weekday = day_of_week.index(item["period"][:3]) + 1 # 曜日を切り取る
        sch.period = int(item["period"][-1]) # 時限を切り取る
        sch.semester = item["semester"]
        sch.room = item["classroom"]
        sch.name = item["name"]
        ret.append(sch)
    return ret

def fetch_from_toyoapi(username, password) -> str:
    import ssl
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    try:
        auth = json.dumps({"instance_id":"eEA6c3ctKjc", "user_id":username, "password":password}).encode('utf-8')
        req = Request('https://light-api.portal.ac/auth/login')
        req.add_header('X-University-Token', 'Zk3nT7Qz6BST49bsjNd9DE8xJs2yNdptlDe20csjvJxumyME')
        req.add_header('Content-Type', 'application/json')
        content = urlopen(req, data=auth, context=ctx).read()

        content = json.loads(content)
        if not content['is_successful']:
            raise Exception(content['result']['message'])

        auth = content['result']['token']
        req = Request('https://light-api.portal.ac/class-schedule/my-schedule')
        req.add_header('X-University-Token', 'Zk3nT7Qz6BST49bsjNd9DE8xJs2yNdptlDe20csjvJxumyME')
        req.add_header('X-Login-Token', auth)
        content = urlopen(req, context=ctx).read()

        content = json.loads(content)
        if not content['is_successful']:
            raise Exception(content['code'])
        return content['result']['items']
    except URLError as e: raise Exception(e.__str__())

def extract_from_toyoapi(obj) -> List[Schedule]:
    ret = []

    for item in obj:
        sch = Schedule()
        # {"day_of_week":1,"time_number":5,"class_name":"コンピュータ・サイエンス概論Ⅱ①,②","room":"２３１３教室"}
        sch.code = item["options"]["授業コード"] # 急に日本語のプロパティ名
        sch.weekday = int(item["day_of_week"])
        sch.period = int(item["time_number"])
        sch.semester = item["options"]["学期"]
        sch.room = item["room"]
        sch.name = item["class_name"]
        ret.append(sch)
    return ret

def register_schedule(student, schedule):
    kourses = dict()

    if student.registered == True:  # 時間割の再登録
        Enrollments.objects.filter(student=student).delete()

    for sch in schedule:
        if sch.code not in kourses:
            kourses[sch.code], new = Courses.objects.get_or_create(
                code=sch.code)
            if new:
                kourses[sch.code].name = sch.name
                kourses[sch.code].save()

        Enrollments.objects.create(
            student=student, course=kourses[sch.code], weekday=sch.weekday, period=sch.period, semester=sch.semester)

    student.registered = True
    student.save()
