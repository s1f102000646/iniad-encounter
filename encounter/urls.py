from django.urls import include, path
from . import views
from encounter import views as encounter_views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', encounter_views.index, name='index'),

    path('auth/', include('social_django.urls', namespace='social')),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('course/<int:weekday>/<int:period>/', encounter_views.course, name='course'),

    path('register/', encounter_views.register, name='register'),
    path('friends/', encounter_views.friends, name='friends'),
    path('friends/code', encounter_views.friend_code, name='friend_code'),
    path('friends/register/', encounter_views.friend_register, name='friend_register'),
    path('settings/', encounter_views.settings, name='settings'),
    path('about/', encounter_views.about, name='about'),
]
