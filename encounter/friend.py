﻿import time, random, json, base64, urllib.parse
from encounter.models import Students


def code(request):
    dump = json.dumps(
        [request.user.email, time.time(), random.randint(0, 100000000)]
    ).encode()
    encode = base64.urlsafe_b64encode(dump).decode("utf-8").replace("=", "-")
    qr_key=encode
    #qr_key = urllib.parse.quote(encode)
    print(f"qr_key={qr_key}")

    user = Students.objects.get(pk=request.user.email)
    user.qr = qr_key
    user.save()
    return qr_key


def search(qr_key):
    try:
        load = json.loads(base64.urlsafe_b64decode(qr_key.replace("-","=")))
    except: return {"isValid":False,"mail":"invalid","name":"invalid"}
    user = Students.objects.get(pk=load[0])
    if user.qr == qr_key:
        return {"isValid":True,"mail":load[0],"name":user.name,"qr_key":qr_key}
    else:
        return {"isValid":False}

def regist(qr_key,mail):
    decode = base64.urlsafe_b64decode(qr_key.replace("-","="))
    load = json.loads(decode.decode('utf-8'))
    print(f"Loaded JSON:{load}")
    user = Students.objects.get(pk=load[0])
    if user.qr == qr_key:
        target=Students.objects.get(pk=mail)
        friend_list=json.loads(target.friends)
        if load[0] in friend_list:return -1
        friend_list.append(load[0])
        target.friends=json.dumps(friend_list)
        target.save()
        return 0
    else:
        return -1