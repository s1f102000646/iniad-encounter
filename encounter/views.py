from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse

import json

from . import friend, timetable
from .models import Students, Enrollments

# Create your views here.


def index(request):
    if not request.student.registered:
        return redirect('register')

    courses = [list() for _ in range(0, 6)]
    enrls_mgr = Enrollments.objects

    myself = enrls_mgr.filter(
        student__mail=request.user.email).filter(in_person=True)
    friends = enrls_mgr.filter(
        student__mail__in=json.loads(request.student.friends)).filter(in_person=True)

    # 同じ授業を受けている
    course_matches = {c['course_id']: None for c in myself.values(
        'course_id').intersection(friends.values('course_id'))}

    # 曜日と時間が一致
    timespan_matches = {f"{c['weekday']}{c['period']}": None for c in myself.values('weekday', 'period').intersection(
        friends.values('weekday', 'period'))}

    # 友達はその時限に在校、自分は空きコマ
    meetable = {f"{c['weekday']}{c['period']}": None for c in friends.values('weekday', 'period').difference(
        myself.values('weekday', 'period'))}

    enrls = enrls_mgr.filter(student=request.student).only(
        'period', 'weekday', 'course__code', 'course__name').order_by('period', 'weekday')
    alias = request.student.course_alias
    for e in enrls:
        status = 'warning' if e.course.code in course_matches else \
            'success' if f"{e.weekday}{e.period}" in timespan_matches else \
            'info' if f"{e.weekday}{e.period}" in meetable else None
        courses[e.period - 1].append({'weekday': e.weekday,
                                      'period': e.period,
                                      # 科目の別名
                                      'name': alias[e.course.code] if e.course.code in alias else e.course.name,
                                      'status': status})

    for i in range(len(courses)):  # テーブルずれ補正
        if len(courses[i]) < 6:
            for w in set(range(1, 6)).difference(set([e['weekday'] for e in courses[i]])):
                courses[i].insert(w - 1, {})

    return render(request, 'encounter/index.html', {'courses': courses})


def course(request, weekday, period):
    same_time_friends_name = []
    same_class_friends_name = []

    my_enrollment = Enrollments.objects.get(
        student__mail=request.user.email, weekday=weekday, period=period)

    if request.method == 'POST':  # 対面授業登録
        if request.POST['alias'] != '':
            request.student.course_alias[my_enrollment.course.code] = request.POST['alias']
            request.student.save()
        my_enrollment.in_person = 'in_person' in request.POST
        my_enrollment.save()
        return redirect("index")

    meetable_courses = Enrollments.objects.filter(student__mail__in=json.loads(
        request.student.friends), in_person=True, weekday=weekday, period=period).only('course__code', 'student__name')

    for c in meetable_courses:
        if c.course.code == my_enrollment.course.code:
            same_class_friends_name.append(c.student.name)
        else:
            same_time_friends_name.append(c.student.name)

    context = {
        'same_time_friends': same_time_friends_name,
        'same_class_friends': same_class_friends_name,
        'in_person': my_enrollment.in_person,
        'weekday': weekday,
        'period': period,
        'alias': request.student.course_alias.get(my_enrollment.course.code, '')
    }

    return render(request, 'encounter/course.html', context)


def register(request):
    if request.method == 'POST':
        schs = []
        if request.GET['method'] == 'iniadded':
            schs = timetable.extract_from_iniadded(request.POST['coursedata'])
            timetable.register_schedule(request.student, schs)
            return redirect('index')
        elif request.GET['method'] == 'toyoapi':
            try:
                jsonstr = timetable.fetch_from_toyoapi(
                    request.user.username.replace('f', 'F'), request.POST['password'])
                schs = timetable.extract_from_toyoapi(jsonstr)
                timetable.register_schedule(request.student, schs)
                return redirect('index')
            except Exception as e:
                return render(request, 'encounter/register.html', {"error": e.__str__()})

    return render(request, 'encounter/register.html')


def friends(request):
    friend_list = json.loads(request.student.friends)

    if request.method == 'POST':  # フレンド削除
        friend_list.remove(request.POST['delete'])
        request.student.friends = json.dumps(friend_list)
        request.student.save()

    friendlst = []
    for email in friend_list:
        name = Students.objects.only('name').get(mail=email).name
        friendlst.append({'email': email, 'name': name})

    return render(request, 'encounter/friends.html', {'friends': friendlst})


def friend_code(request):
    fcode = friend.code(request)
    register_url = request.build_absolute_uri(
        reverse('friend_register')) + "?fcode=" + fcode
    return render(request, 'encounter/friends/friend_code.html', {"qr_url": f"https://api.qrserver.com/v1/create-qr-code/?data={register_url}"})


def friend_register(request):
    if "fcode" in request.GET:  # QRコードが読み取られた時
        search = friend.search(request.GET["fcode"])
        if search["isValid"] and (search["mail"] != request.user.email):  # 自分と友達になることはない
            return render(request, 'encounter/friends/friend_register.html', search)
    elif "fcode" in request.POST:  # 確認画面で「はい」を押した時
        return render(request, 'encounter/friends/friend_confirm.html', {"result": friend.regist(request.POST["fcode"], request.user.email)})

    return redirect("index")


def settings(request):
    if 'dispname' in request.POST:
        request.student.name = request.POST['dispname']
        request.student.save()
    return render(request, 'encounter/settings.html')

def about(request):
    return render(request, 'encounter/about.html')