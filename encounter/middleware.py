from django.shortcuts import redirect, reverse
from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject

from .models import Students


def get_first_path(path):
    return path.lstrip('/').split('/')[0]


oauth_url = reverse('social:begin', kwargs={'backend': 'google-oauth2'})
oauth_first_path = get_first_path(oauth_url)


class LoginRequiredMiddleware(MiddlewareMixin):
    def process_request(self, request):
        current_first_path = get_first_path(request.path_info)

        # auth/hogeを除外
        if current_first_path != oauth_first_path and not request.user.is_authenticated:
            return redirect(oauth_url)


class LookupStudentMiddleware(MiddlewareMixin):
    @staticmethod
    def get_student(request):
        if not hasattr(request, '_cached_student') and request.user.is_authenticated:
            request._cached_student, created = Students.objects.get_or_create(
                pk=request.user.email)
            if created:
                request._cached_student.name = f'{request.user.first_name} {request.user.last_name}'
                request._cached_student.save()
        return request._cached_student

    def process_request(self, request):
        request.student = SimpleLazyObject(
            lambda: LookupStudentMiddleware.get_student(request))
