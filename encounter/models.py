from django.db import models

# Create your models here.


class Students(models.Model):
    mail = models.EmailField(max_length=22, primary_key=True)
    name = models.CharField(max_length=20)
    registered = models.BooleanField(default=False)
    qr = models.CharField(max_length=200)
    friends = models.TextField(default='[]')
    course_alias = models.JSONField(default=dict)


class Courses(models.Model):
    code = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=200)


class Enrollments(models.Model):
    student = models.ForeignKey(Students, on_delete=models.CASCADE)
    course = models.ForeignKey(Courses, on_delete=models.CASCADE)
    weekday = models.IntegerField()
    period = models.IntegerField()
    in_person = models.BooleanField(default=False) # 対面授業
    semester = models.CharField(max_length=6)

    # class Meta:
    #     unique_together = (("student", "course", "weekday"),)
